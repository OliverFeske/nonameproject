﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon
{
	[SerializeField] private float pelletCount;

	#region Override Methods
	public override void ShootOneTime()
	{
		// play audio
		source.PlayOneShot(shootingSound);

		// play shooting effect
		playerManager.WC.ShootEffect(shootingEffect, secondShootingEffect, fireRate);

		// subtract 1 bullet from the magazine
		currentAmmo--;
		playerUI.PlayerWeaponUI();

		for (int i = 0; i < pelletCount; i++)
		{
			// get a direction with applied spray 
			Vector3 direction = GetSpray();

			RaycastHit hit;
			Debug.DrawRay(rayCastTransformPoint.position, direction * bulletRange, Color.blue, 10f);
			if (Physics.Raycast(rayCastTransformPoint.position, direction, out hit, bulletRange, hitLayer))
			{
				// set a hit effect at the hitPoint
				UseHitEffect(weaponparticleEffectsPools.ShotgunHitEffects, hit.normal, hit.point);

				// only draw a shooting line if the hitpoint is far away
				if (Vector3.Distance(hit.point, firePoint.position) > playerWeaponControls.MinRangeForEffect)
				{
					// first person weapon trail
					UseLightTrail(weaponparticleEffectsPools.ShotgunTrails, firePoint, hit.point, playerManager.SelfLayer);

					// third person weapon trail
					UseLightTrail(weaponparticleEffectsPools.ShotgunTrails, secondFirePoint, hit.point, playerManager.OtherPlayerLayer);
				}

				// if you hit a player, apply damage
				if (hit.collider.CompareTag("Player"))
				{
					hit.collider.gameObject.transform.root.GetComponent<PlayerHealthControl>().GetDamage(damage);

					// play a different sound when a player is hit
					AudioSource.PlayClipAtPoint(humanHitSound, hit.point);
				}
				else if (hit.collider.CompareTag("HeadCollider"))
				{
					hit.collider.gameObject.transform.root.GetComponent<PlayerHealthControl>().GetDamage(damage * headshotMultiplier);
				}
				else
				{
					// play default hit sound
					AudioSource.PlayClipAtPoint(defaultHitSound, hit.point);
				}
			}
			else
			{
				// first person weapon trail
				Vector3 virtualHitPos = firePoint.position + direction * 100f;
				UseLightTrail(weaponparticleEffectsPools.ShotgunTrails, firePoint, virtualHitPos, playerManager.SelfLayer);

				// third person weapon trail
				Vector3 secondVirtualHitPos = secondFirePoint.position + direction * 100f;
				UseLightTrail(weaponparticleEffectsPools.ShotgunTrails, firePoint, secondVirtualHitPos, playerManager.OtherPlayerLayer);
			}
		}
	}
	#endregion
}