﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEnvironmentInteraction : MonoBehaviour
{
	#region Script References
	private PlayerWeaponControls playerWeaponControls;
	private PlayerHealthControl playerHealthControl;
	private PlayerMovement playerMovement;
	private PlayerUI playerUI;
	#endregion
	#region Tag Strings
	[SerializeField] private string healthPackTag = "Healthpack";
	[SerializeField] private string assaultRifleAmmoTag = "ARAmmo";
	[SerializeField] private string shotgunAmmoTag = "ShotgunAmmo";
	[SerializeField] private string energyAmmoTag = "EnergyAmmo";
	[SerializeField] private string jumpPadString = "JumpPad";
	#endregion
	#region Variables
	private CapsuleCollider playerCollider;
	#endregion

	#region Player Standard Methods
	public void Init(PlayerWeaponControls _playerWeaponControls, PlayerHealthControl _playerHealthControl, PlayerMovement _playerMovement, PlayerUI _playerUI)
	{
		playerCollider = GetComponent<CapsuleCollider>();
		playerWeaponControls = _playerWeaponControls;
		playerHealthControl = _playerHealthControl;
		playerMovement = _playerMovement;
		playerUI = _playerUI;
		Debug.Log("PlayerEnvironmentInteraction has succesfully been initialised");
	}
	public void OnUpdate()
	{

	}
	#endregion

	#region MonoBehaviour
	// DO STH ABOUT THIS PART BECAUSE IT IS TOO LONG AND NOT EFFICIENT
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag(assaultRifleAmmoTag))
		{
			// FILL AMMO OF TYPE TO PLAYER
			var weapon = playerWeaponControls.Weapons[0];

			if (weapon == null)
				return;
			else
				if (weapon.RefillMaxAmmo()) { DeactivateObject(other.gameObject); }

			playerUI.PickUpMagazin(0);

			Debug.Log("Refill Ammo of Type: AssaultRifle");
		}
		else if (other.CompareTag(shotgunAmmoTag))
		{
			// FILL AMMO OF TYPE TO PLAYER
			var weapon = playerWeaponControls.Weapons[1];

			if (weapon == null)
				return;
			else
				if (weapon.RefillMaxAmmo()) { DeactivateObject(other.gameObject); }

			playerUI.PickUpMagazin(1);

			Debug.Log("Refill Ammo of Type: Shotgun");
		}
		else if (other.CompareTag(energyAmmoTag))
		{
			// FILL AMMO OF TYPE TO PLAYER
			var weapon = playerWeaponControls.Weapons[2];

			if (weapon == null)
				return;
			else
				if (weapon.RefillMaxAmmo()) { DeactivateObject(other.gameObject); }

			playerUI.PickUpMagazin(2);

			Debug.Log("Refill Ammo of Type: Energy");
		}
		else if (other.CompareTag(healthPackTag))
		{
			if (playerHealthControl.RefillHealth()) { DeactivateObject(other.gameObject); }
            playerUI.PlayerHealthUI();
			Debug.Log("Refill Health of Player");
		}
		else if (other.CompareTag(jumpPadString))
		{
			JumpPad jumpGO = other.gameObject.GetComponent<JumpPad>();
			float jumpHeight = jumpGO.JumpHeight;
			jumpGO.DeactivatePad();
			playerMovement.AddJumpPadForce(jumpHeight);
		}
	}
	#endregion

	#region Destroy Object?
	void DeactivateObject(GameObject other)
	{
		other.GetComponent<PickUpItem>().DeactivateItem();
	}
	#endregion
}
