﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthControl : MonoBehaviour
{
	#region Public Variables
	[SerializeField] private float maxHealth;
	public float MaxHealth { get => maxHealth; }
	[SerializeField] private float currentHealth;
	public float CurrentHealth { get => currentHealth; }
	[SerializeField] private float healthRefillvalue;
	public float HealthRefillValue { get => healthRefillvalue; }
	private bool isDead;
	public bool IsDead { get => isDead; }
	#endregion
	#region Variables
	private PlayerReset playerReset;
	private PlayerWeaponControls playerWeaponControls;
	private PlayerUI playerUI;
	#endregion

	#region Player Standard Methods
	public void Init(PlayerReset _playerReset, PlayerWeaponControls _playerWeaponControls, PlayerUI _playerUI)
	{
		playerReset = _playerReset;
		playerWeaponControls = _playerWeaponControls;
		playerUI = _playerUI;
		currentHealth = maxHealth;
		Debug.Log("PlayerHealthControl has been successfully initialised");
	}
	public void OnUpdate()
	{
	}
	#endregion

	#region Public Methods
	public void GetDamage(float damage)
	{
		currentHealth -= damage;
		playerUI.PlayerHealthUI();
		Debug.Log($"{gameObject.name} got {damage} Damage!");
		if (currentHealth <= 0)
		{
			Debug.Log($"{gameObject.name} is dead!");
			currentHealth = 0;
			playerUI.PlayerHealthUI();
			playerWeaponControls.ResetWeapons();
			playerReset.OnPlayerDeath();
		}
	}
	public void RestoreHealth()
	{
		currentHealth = maxHealth;
	}
	public bool RefillHealth()
	{
		if (currentHealth == maxHealth)
		{
			return false;
		}
		else
		{
			currentHealth += healthRefillvalue;
			currentHealth = Mathf.Clamp(currentHealth, 0f, maxHealth);
			return true;
		}
	}
	#endregion
}
