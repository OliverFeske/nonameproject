﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItem : MonoBehaviour
{
	[SerializeField] private GameObject item;
	[SerializeField] private GameObject deactivationEffect;
	[SerializeField] private GameObject reactivationEffect;
	[SerializeField] private float reactivateTime;
	[SerializeField] private float rotationSpeed = 10f;

	private SphereCollider col;

	#region MonoBehaviour
	void Start()
	{
		col = GetComponent<SphereCollider>();
	}
	void Update()
	{
		item.transform.Rotate(new Vector3(0f, Time.deltaTime * rotationSpeed, 0f));
	}
	#endregion

	#region Public Methods
	public void DeactivateItem()
	{
		StartCoroutine("Deactivate");
	}
	#endregion

	#region Coroutines
	IEnumerator Deactivate()
	{
		item.SetActive(false);
		col.enabled = false;
		reactivationEffect.SetActive(false);
		deactivationEffect.SetActive(true);

		yield return new WaitForSeconds(reactivateTime);

		deactivationEffect.SetActive(false);
		reactivationEffect.SetActive(true);
		item.SetActive(true);
		col.enabled = true;

	}
	#endregion
}
