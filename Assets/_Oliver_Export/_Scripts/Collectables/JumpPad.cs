﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPad : MonoBehaviour
{
	[Header("Jump Height")]
	[SerializeField] private float jumpHeight;
	public float JumpHeight { get => jumpHeight; }
	[Space][Header("Arrow Controls")]
	[SerializeField] private Transform hoveringArrows;
	[SerializeField] private float hoverFactor;
	[SerializeField] private float maxHeigth;
	[Space][Header("Deactivation Settings")]
	[SerializeField] private GameObject rings;
	[SerializeField] private SphereCollider col;
	[SerializeField] private float waitTime;
	private Vector3 startPos;
	private WaitForSeconds wfs;

	void Start()
	{
		startPos = hoveringArrows.transform.position;
		wfs = new WaitForSeconds(waitTime);
	}
	void Update()
	{
		hoveringArrows.position += new Vector3(0f, Time.deltaTime * hoverFactor, 0f);
		if(hoveringArrows.position.y >= maxHeigth + startPos.y)
		{
			hoveringArrows.position = startPos;
		}
	}

	public void DeactivatePad()
	{
		StartCoroutine(DeactivatePadForTime());
	}

	IEnumerator DeactivatePadForTime()
	{
		col.enabled = false;
		rings.SetActive(false);
		hoveringArrows.gameObject.SetActive(false);

		yield return wfs;

		col.enabled = true;
		rings.SetActive(true);
		hoveringArrows.gameObject.SetActive(true);
	}
}
