﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAudio : MonoBehaviour
{
    [Header("Players")]
    [SerializeField] private GameObject Player1;
    [SerializeField] private GameObject Player2;

    [Header("AudioSources")]
    [SerializeField] private AudioSource[] AudioPlayer1;
    [SerializeField] private AudioSource[] AudioPlayer2;

    [SerializeField] private float MinimumDistanceForSound = 100;
    void Start()
    {
        //Set each Audioplayer to its respective Speaker ( Right / Left)
        for (int i = 0; i < AudioPlayer1.Length; i++)
        {
            AudioPlayer1[i].panStereo = -1;
        }
        for (int i = 0; i < AudioPlayer2.Length; i++)
        {
            AudioPlayer2[i].panStereo = 1;
        }
    }

    void Update()
    {
        //Calculate the distance between both players. 
        float DistanceBetweenPlayer = Vector3.Distance(Player1.transform.position, Player2.transform.position);

        //Divide the DistanceBetweenPlayers by the Minimum Distance the players have to be inside for the sound to be heard
        for (int i = 0; i < AudioPlayer1.Length; i++)
        {
            AudioPlayer1[i].panStereo = DistanceBetweenPlayer / MinimumDistanceForSound;
        }
        for (int i = 0; i < AudioPlayer2.Length; i++)
        {
            AudioPlayer2[i].panStereo = -DistanceBetweenPlayer / MinimumDistanceForSound;
        }

        

    }
}
