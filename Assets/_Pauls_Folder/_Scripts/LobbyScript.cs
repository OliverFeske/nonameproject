﻿using UnityEngine;
using TMPro;

public class LobbyScript : MonoBehaviour
{
    [SerializeField] private GameObject LobbyPanel1;
    [SerializeField] private GameObject LobbyPanel2;

    [SerializeField] private GameObject PlayerPanel1;
    [SerializeField] private GameObject PlayerPanel2;

    [SerializeField] private GameObject PlayerStartPanel1;
    [SerializeField] private GameObject PlayerStartPanel2;

    [SerializeField] private TextMeshProUGUI Player1StartTxt;
    public TextMeshProUGUI StartTXT1 { get => Player1StartTxt; }
    [SerializeField] private TextMeshProUGUI Player2StartTxt;
    public TextMeshProUGUI StartTXT2 { get => Player2StartTxt; }

    private bool gameHasStarted = false;

    public string[] Controllers;
    private bool foundController1 = false;
    private bool foundController2 = false;

    private int numOfControllers = 0;
    private bool Player1Ready = false;
    private bool Player2Ready = false;
    public GAME game;

    private void Awake()
    {
        numOfControllers = 0;
    }
    void Start()
    {
        LobbyPanel1.SetActive(true);
        PlayerPanel1.SetActive(false);

        LobbyPanel2.SetActive(true);
        PlayerPanel2.SetActive(false);
    }

    public void PrepareRestartGame()
    {
        PlayerStartPanel1.SetActive(true);
        PlayerStartPanel2.SetActive(true);
        Player1StartTxt.text = "Press Y to restart";
        Player2StartTxt.text = "Press Y to restart";
    }

    public void RestartGame()
    {
        PlayerStartPanel1.SetActive(false);
        PlayerStartPanel2.SetActive(false);
    }


    void Update()
    {
        if (Player1Ready == true && Player2Ready == true)
        {
            if(gameHasStarted == true) { return; }
            gameHasStarted = true;
            Debug.Log("The Game has Started");
            PlayerStartPanel1.SetActive(false);
            PlayerStartPanel2.SetActive(false);
            game.ChangeGameState();
        }

        if (Input.GetButtonDown("SelectP1"))
        {
            Player1Ready = true;
            Player1StartTxt.text = "Ready";
        }

        if (Input.GetButtonDown("SelectP2"))
        {
            Player2Ready = true;
            Player2StartTxt.text = "Ready";
        }

        Controllers = Input.GetJoystickNames();
        for (int i = 0; i < Controllers.Length; i++)
        {
            if (Controllers[i].Length != 33)
            {
                return;
            }

            if (!foundController1 && Controllers.Length == 1)
            {
                numOfControllers = 1;
                foundController1 = true;
            }

            if(!foundController2 && Controllers.Length == 2)
            {
                foundController1 = true;
                foundController2 = true;
            }

            //Debug.Log("NumOfControllers Is " + numOfControllers);
        }
        
        if (foundController1)
        {
            //Debug.Log("Player 1 has joined");
            LobbyPanel1.SetActive(false);
            PlayerPanel1.SetActive(true);
           //Activate Player Controls for player1
        }

        if (foundController2)
        {
           // Debug.Log("Player 2 has joined");
            LobbyPanel2.SetActive(false);
            PlayerPanel2.SetActive(true);
            //Activate Player Controls for Player 2
        }
    }


}
