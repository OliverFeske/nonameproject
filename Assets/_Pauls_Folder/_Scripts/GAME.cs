﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GAME : MonoBehaviour
{
    public enum GameStates { Lobby, Gamestart, GamePlay, GameFinish }

    private GameStates gamestate = GameStates.Lobby;

    [SerializeField] private PlayerReset PlayerReset1;
    [SerializeField] private PlayerReset PlayerReset2;
    [SerializeField] private PlayerUI playerUI_1;
    [SerializeField] private PlayerUI playerUI_2;
    [SerializeField] private LobbyScript Lobby;

    [SerializeField] private LobbyScript lobby;

    [SerializeField] private int player1Lives;
    public int Player1Lives { get => player1Lives; }

    [SerializeField] private int player2Lives;
    public int Player2Lives { get => player2Lives; }

    private float gameTimer = 300;
    public float GameTimer { get => gameTimer; set => gameTimer = value; }

    private float startTimer;
    public float StartTimer { get => startTimer; set => startTimer = value; }

    [SerializeField] private float timerGame;
    [SerializeField] private float TimerStart;
    [SerializeField] private float RestarTimer;

    private bool player1Ready = false;
    private bool player2Ready = false;
    private bool RestartGame = false;

    private void Awake()
    {
        gamestate = GameStates.Lobby;
        player1Ready = false;
        player2Ready = false;
        RestartGame = false;
    }


    void Update()
    {
        if (gamestate == GameStates.Lobby)
        {
            //nothing happens here waiting for player to start the game
            playerUI_1.InfinitLives();
            playerUI_2.InfinitLives();
        }
        else if (gamestate == GameStates.Gamestart)
        {
            startTimer = TimerStart -= Time.deltaTime;
            int seconds = ((int)startTimer) % 60;
            playerUI_1.AnnoucementTextUI("Game starts in " + ((seconds < 10) ? "0" + seconds : seconds.ToString()));
            playerUI_2.AnnoucementTextUI("Game starts in " + ((seconds < 10) ? "0" + seconds : seconds.ToString()));

            if (startTimer <= 0)
            {
                playerUI_1.AnnoucementTextUI("");
                playerUI_2.AnnoucementTextUI("");
                playerUI_1.PlayerKills();
                playerUI_2.PlayerKills();
                PlayerReset1.ResetOnStart();
                PlayerReset2.ResetOnStart();
                gamestate = GameStates.GamePlay;
                Debug.Log("got here");
            }
        }
        else if (gamestate == GameStates.GamePlay)
        {
            gameTimer = timerGame -= Time.deltaTime;
        }
        else if (gamestate == GameStates.GameFinish)
        {
            if(RestartGame == false)
            {
                lobby.PrepareRestartGame();
                RestartGame = true;
            }
            
            if (Input.GetButtonDown("SelectP1"))
            {
                lobby.StartTXT1.text = "Ready";
                player1Ready = true;
            }

            if (Input.GetButtonDown("SelectP2"))
            {
                lobby.StartTXT2.text = "Ready";
                player2Ready = true;
            }

            if (player1Ready == true && player2Ready == true)
            {
                lobby.RestartGame();
                float Restart = RestarTimer -= Time.deltaTime;
                int seconds = ((int)Restart) % 60;
                playerUI_1.AnnoucementTextUI("Game starts in " + ((seconds < 10) ? "0" + seconds : seconds.ToString()));
                playerUI_2.AnnoucementTextUI("Game starts in " + ((seconds < 10) ? "0" + seconds : seconds.ToString()));

                if (RestarTimer <= 0)
                {
                    SceneManager.LoadScene("2PlayerScene");
                    
                }
                
            }

        }

        if (gameTimer <= 0)
        {
            if (player1Lives > player2Lives)
            {
                gamestate = GameStates.GameFinish;
                playerUI_1.WonGameUI();
                playerUI_2.LostGame();
            }
            else if (player2Lives > player1Lives)
            {
                gamestate = GameStates.GameFinish;
                playerUI_2.WonGameUI();
                playerUI_1.LostGame();
            }

            else if (player1Lives == player2Lives)
            {
                timerGame += 60;
            }
            LivesCounter();
        }
    }

    public void KillCounter(int PlayerIndex)
    {
        if (gamestate != GameStates.GamePlay) { return; }

        if (PlayerIndex == 1)
        {
            player1Lives--;
            playerUI_1.PlayerKills();
            playerUI_2.PlayerKills();
            LivesCounter();

        }
        else if (PlayerIndex == 2)
        {
            player2Lives--;
            playerUI_1.PlayerKills();
            playerUI_2.PlayerKills();
            LivesCounter();

        }
    }
    void LivesCounter()
    {
        if (player1Lives == 0)
        {
            gamestate = GameStates.GameFinish;
            playerUI_2.WonGameUI();
            playerUI_1.LostGame();
        }
        else if (Player2Lives == 0)
        {
            gamestate = GameStates.GameFinish;
            playerUI_1.WonGameUI();
            playerUI_2.LostGame();
        }
    }

    public void ChangeGameState()
    {
        gamestate = GameStates.Gamestart;
    }

    //public IEnumerator GameStart()
    //{
    //    //Fade message in and out
    //}
    //public IEnumerator GameEnd()
    //{
    //    //Game over message and fade in the Victory/GameLost Screens
    //}
    //public IEnumerator OverTime()
    //{
    //    // Fade message in and Out
    //}

}
