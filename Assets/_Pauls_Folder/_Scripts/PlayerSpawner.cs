﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    [SerializeField] private SpawnPointAlert[] spawnPoints;

    private int randomSpawnPoint;
    
    void Start()
    {
		//foreach (SpawnPointAlert spawnpoint in transform)
		//{
		//    spawnPoints.Add(spawnpoint);
		//}

		spawnPoints = GetComponentsInChildren<SpawnPointAlert>();
    }

    public Transform SpawnPlayer()
    {
        SpawnPointAlert currentSpawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
        
        while (currentSpawnPoint.PlayerInside)
        {
           currentSpawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
        }
        return currentSpawnPoint.transform;
          
    }
}
