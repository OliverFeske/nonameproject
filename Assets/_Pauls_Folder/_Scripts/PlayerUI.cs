﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerUI : MonoBehaviour
{
	[Header("Health")]
	[SerializeField] private Image PlayerHealthBar;
	[SerializeField] private TextMeshProUGUI PlayerHealthBarCount;

	[Header("Ammunition")]
	[SerializeField] private TextMeshProUGUI LoadedAmmo;


	[Header("SelectedWeapon")]
	[SerializeField] private TextMeshProUGUI[] WeaponAmmoText;
	[SerializeField] private Image[] WeaponSelectedBar;
	public Image[] WeaponsUIImage;

	[Header("WeaponUIColor")]
	[SerializeField] private Color FullUIColor;
	[SerializeField] private Color HalfUIColor;
	[SerializeField] private Color NoColor;

	[Header("Timer")]
	[SerializeField] private TextMeshProUGUI Timer;

	[Header("PlayerKills")]
	[SerializeField] private TextMeshProUGUI Player1KillsUI;
	[SerializeField] private TextMeshProUGUI Player2KillsUI;

	[Header("AnnoucementUI & EndGameUI")]
	[SerializeField] private TextMeshProUGUI AnnoucementUI;
	[SerializeField] private GameObject VictoryUI;
	[SerializeField] private GameObject GameOverUI;

    [Header("MenuUI")]
    [SerializeField] private GameObject Menu;

	private PlayerWeaponControls weaponscontrol;
	private GAME game;
	private PlayerHealthControl playerhealthcontrol;
    private bool MenuOpened = false;

	public void Init(PlayerHealthControl _playerhealthcontrol, PlayerWeaponControls _weaponscontrol, GAME _game)
	{
        SetDefaults();
        playerhealthcontrol = _playerhealthcontrol;
        weaponscontrol = _weaponscontrol;
        game = _game;
		VictoryUI.gameObject.SetActive(false);
		GameOverUI.gameObject.SetActive(false);
	}
	public void OnUpdate()
	{
		int minutes = ((int)game.GameTimer) / 60;
		int seconds = ((int)game.GameTimer) % 60;

		Timer.text = minutes + ":" + ((seconds < 10) ? "0" + seconds : seconds.ToString());
	}

    void SetDefaults()
    {
        MenuOpened = false;
        SensitivityMenu();
        PlayerHealthBar.fillAmount = 1;
        PlayerHealthBarCount.text = "100/100";
        LoadedAmmo.text = "30/30";
        WeaponsUIImage[0].color = FullUIColor;
        WeaponAmmoText[0].color = FullUIColor;
        WeaponSelectedBar[1].color = NoColor;
        WeaponSelectedBar[2].color = NoColor;
        WeaponAmmoText[0].text = "0"; //+ weaponscontrol.CurrentWeapon.CurrentMaxAmmo;
        WeaponAmmoText[1].text = "0"; //+ weaponscontrol.CurrentWeapon.CurrentMaxAmmo;
        WeaponAmmoText[2].text = "0"; //+ weaponscontrol.CurrentWeapon.CurrentMaxAmmo;
        AnnoucementUI.text = "";

    }

    public void AnnoucementTextUI(string text)
	{
		AnnoucementUI.text = text;
	}

	public void PlayerHealthUI()
	{
		PlayerHealthBar.fillAmount = playerhealthcontrol.CurrentHealth / playerhealthcontrol.MaxHealth;
		PlayerHealthBarCount.text = playerhealthcontrol.CurrentHealth + "/" + playerhealthcontrol.MaxHealth;
	}

	public void PlayerWeaponUI()
	{
		LoadedAmmo.text = weaponscontrol.CurrentWeapon.CurrentAmmo + "/" + weaponscontrol.CurrentWeapon.MagazineSize;
	}

    public void ResetWeaponAmmo()
    {
        for (int i = 0; i < WeaponAmmoText.Length; i++)
        {
            WeaponAmmoText[i].text = "" + weaponscontrol.CurrentWeapon.CurrentMaxAmmo;
        }
    }

	public void ReloadWeapon(int currenWeapon)
	{
		WeaponAmmoText[currenWeapon].text = "" + weaponscontrol.CurrentWeapon.CurrentMaxAmmo;
		PlayerWeaponUI();
	}

    public void PickUpMagazin(int idx)
    {
        WeaponAmmoText[idx].text = "" + weaponscontrol.Weapons[idx].CurrentMaxAmmo; 
    }

	public void PlayerKills()
	{
		Player1KillsUI.text = "" + game.Player1Lives;
		Player2KillsUI.text = "" + game.Player2Lives;
	}

	public void WonGameUI()
	{
		VictoryUI.gameObject.SetActive(true);
	}

	public void LostGame()
	{
		GameOverUI.gameObject.SetActive(true);
	}
	public void SwitchingWeaponUI(int currentWeapon, int oldWeapon)
	{
        PlayerWeaponUI();

        WeaponsUIImage[currentWeapon].color = FullUIColor;
		WeaponSelectedBar[currentWeapon].color = FullUIColor;
		WeaponAmmoText[currentWeapon].color = FullUIColor;

		WeaponsUIImage[oldWeapon].color = HalfUIColor;
		WeaponSelectedBar[oldWeapon].color = NoColor;
		WeaponAmmoText[oldWeapon].color = HalfUIColor;
	}

    public void SensitivityMenu()
    {
        if(MenuOpened == true)
        {
            MenuOpened = false;
            Menu.SetActive(true);
        }
        else if(MenuOpened == false)
        {
            Menu.SetActive(false);
            MenuOpened = true;
        }
    }

    public void InfinitLives()
    {
        Player1KillsUI.text = "0";
        Player2KillsUI.text = "0";
    }
}
