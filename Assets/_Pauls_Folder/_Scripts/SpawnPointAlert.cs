﻿using UnityEngine;

public class SpawnPointAlert : MonoBehaviour
{
    private bool playerInside;
    public bool PlayerInside { get => playerInside; set => playerInside = value; }

    private int playerCount;
    public int PlayerCount { get => playerCount; set => playerCount = value; }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerInside = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerInside = false;
        }
    }
}
