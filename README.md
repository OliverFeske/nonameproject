# One Of Us

This is the finished group project we worked on. 
I already got the main part done in another side project([Final Exam Personal](https://gitlab.com/GreenPhoenixDev/finalexam_personal)), but this here is the version we put all the things we worked on together.
So I mostly worked on implementing stuff and fixing bugs that happened with 2 players at the same time, mostly Layer related things and coroutines.


Something that gave me a headache at times was that coroutines stopped when changing weapons, so the effects would not be put back in the pools.
So this part right here is the biggest thing I changed,  the rest I tried to leave as it was, because i did not want to create more bugs due to our time limit.

![Weapon Coroutines](https://imgur.com/dGri41N.png)